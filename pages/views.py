#from django.shortcuts import render
# pages/views.py
from django.views.generic import TemplateView


class HomePageView(TemplateView):
  template_name = 'home.html'

class AcercadePageView(TemplateView):
  template_name = 'acercade.html'
# Create your views here.

